import { Component, OnInit } from '@angular/core';
import { HeroesService} from '../../../services/heroes.services';
import { HeroesModel } from '../../models/heroes.models';
import { Router } from '@angular/router';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: [
  ]
})
export class HeroesComponent implements OnInit {
heroes: any[] = [];

  // dispara el constructor de nuestro servicios
  constructor(private heroesService: HeroesService, private router: Router) { }

  ngOnInit() {
    this.heroes = this.heroesService.getHeroes();

  }
  // navegación vía programación
  verHeroe(idx: number){
    this.router.navigate(['/heroe', idx]);

  }

}
