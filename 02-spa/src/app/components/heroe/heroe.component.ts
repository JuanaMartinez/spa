import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../../services/heroes.services';



@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [
  ]
})
export class HeroeComponent  {
  heroe: any = {};
  

  constructor(private activatedRoute: ActivatedRoute, private heroesService: HeroesService) {
    this.activatedRoute.params.subscribe(params => {
      console.log(params.id); // este id viene de la ruta que puse en approuting de heroe/id
      this.heroe = this.heroesService.getHeroe(params.id);
      console.log(this.heroe);

    });
   }

 /*  ngOnInit(): void {
  } */

}
